export default class Item {
  constructor({
    title = 'New item',
    start = new Date().getMonth(),
    end = null,
    person = null
  }) {
    this.title = title
    this.start = start
    this.end = end || (start + 1) % 12
    this.person = person
  }
}
