export default class Board {
  constructor(title = 'New Board') {
    this.title = title
    this.rows = []
  }
}
