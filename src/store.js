import Vue from 'vue'
import Vuex from 'vuex'

import Item from '@/models/Item'

import api from '@/api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    boards: []
  },
  mutations: {
    SET_BOARDS(state, data) {
      state.boards = [...data.boards]
    }
  },
  actions: {
    async fetchBoards({ commit }) {
      const { data } = await api.get('board')
      commit('SET_BOARDS', data)
    },
    async addRow({ state, commit }, payload) {
      const [board] = state.boards
      board.rows.push(new Item(payload))
      commit('SET_BOARDS', { data: { board: [board] } })
    }
  }
})
