import Board from '@/models/Board'
import Item from '@/models/Item'

import { data } from '@/data/gantt'

export default {
  get() {
    return new Promise(resolve => {
      setTimeout(() => {
        // Create a test board
        const board = new Board('Test board')
        board.rows = data.map(item => new Item(item))

        // Only returning 1 board for now
        const boards = [board]
        resolve({ message: 'All boards', data: { boards } })
      }, 2000)
    })
  }
}
