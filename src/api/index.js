import board from './board'

const routes = {
  board
}

export default {
  async get(route) {
    return await routes[route].get()
  },
  async post(route, payload) {
    return await routes[route].post(payload)
  }
}
