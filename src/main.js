import Vue from 'vue'
import VueWait from 'vue-wait'
import App from './App.vue'
import router from './router'
import store from './store'

import '@/assets/main.scss'

Vue.use(VueWait)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  wait: new VueWait({
    useVuex: true
  }),
  render: h => h(App)
}).$mount('#app')
